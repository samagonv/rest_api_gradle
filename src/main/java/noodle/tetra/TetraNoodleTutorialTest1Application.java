package noodle.tetra;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TetraNoodleTutorialTest1Application {

	public static void main(String[] args) {
		SpringApplication.run(TetraNoodleTutorialTest1Application.class, args);
	}
}
